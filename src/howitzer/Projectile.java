package howitzer;
/**
 * Projectile class - Represents a projectile
 * @author - Tirth Patel
 */
public class Projectile {
    public static final double MIN_MASS= 0.01;
    public static final double MAX_MASS= 100.0;
    public static final double MIN_RADIUS= 0.01;
    public static final double MAX_RADIUS= 100.0;
    public static final double MIN_DRAG_COEFFICIENT= 0.01;
    public static final double MAX_DRAG_COEFFICIENT= 3.0;
    public static final double MIN_FLUID_DENSITY= 0.01;
    public static final double MAX_FLUID_DENSITY= 5000.0;


    private double mass;
    private double[] velocity;    // velocity vector (vx, vy, vz)
    private double[] position;    // position vector (x, y, z)
    private double[] gravity;     // gravity vector (gx, gy, gz)
    private double dragCoefficient;
    private double[] force;      // force vector (fx, fy, fz)
    private double fluidDensity;
   // private double projectileArea;
    private double[] flowVelocity; // flow velocity vector (ux, uy, uz)
    private double radius;


    /**
     * Constructor for Projectile class
     * @param mass          - mass of the projectile
     * @param velocity      - velocity vector of the projectile
     * @param position      - position vector of the projectile
     * @param gravity       - gravity vector acting on the projectile
     * @param force         - force vector of the projectile
     * @param dragCoefficient- drag coefficient 
     * @param fluidDensity  - density of the fluid
     * @param flowVelocity  - flow velocity vector of the fluid
     * @param radius        - radius of the projectile
     */
    public Projectile(double mass, double[] velocity, double[] position, double[] gravity, double[] force,double dragCoefficient, double fluidDensity, double[] flowVelocity, double radius) {
        this.mass = mass;
        this.velocity = velocity;
        this.position = position;
        this.gravity = gravity;
        this.force = force;
        this.dragCoefficient = dragCoefficient;
        this.fluidDensity = fluidDensity;
        //this.projectileArea = projectileArea;
        this.flowVelocity = flowVelocity;
        this.radius = radius;

        validateValues();               //validate values
    }

    /**
     * validateValues method - Validates the values of the projectile
     * 
     * This method validates the values of the projectile and throws an exception if the values are not valid
     * This method checks if the values are in the range and if the vectors are of length 3. It also checks if velocity and gravity are not zero vectors
     */
    private void validateValues(){

        validateInRange(mass, "Mass", MIN_MASS, MAX_MASS);
        validateInRange(radius, "Radius", MIN_RADIUS, MAX_RADIUS);
        validateInRange(dragCoefficient, "Drag coefficient", MIN_DRAG_COEFFICIENT, MAX_DRAG_COEFFICIENT);
        validateInRange(fluidDensity, "Fluid density", MIN_FLUID_DENSITY, MAX_FLUID_DENSITY);

        validateVectorLength(velocity, "Velocity", 3);
        validateVectorLength(gravity, "Gravity", 3);
        validateVectorLength(force, "Force", 3);
        validateVectorLength(flowVelocity, "Flow velocity", 3);
        validateVectorLength(position, "Position", 3);

        validateVectorValues(velocity, "Velocity");
        validateVectorValues(gravity, "Gravity");
        validateVectorValues(force, "Force");
        validateVectorValues(flowVelocity, "Flow velocity");
        validateVectorValues(position, "Position");

        //Velocity and gravity must not be zero vectors
        if (VectorProperties.isZeroVector(velocity)){
            throw new IllegalArgumentException("Velocity must not be a zero vector");
        }

        if (VectorProperties.isZeroVector(gravity)){
            throw new IllegalArgumentException("Gravity must not be a zero vector");
        }

    }

    /**
     * validateInRange method - Validates if the value is in the range
     * @param value 
     * @param name
     * @param Min
     * @param Max
     * 
     * Throws an exception if the value is not in the range
     */
    private void validateInRange(double value, String name, double Min, double Max){
        if (value < Min || value > Max){
            throw new IllegalArgumentException(name + " must be between " + Min + " and " + Max);
        }
    }

    /**
     * validateVectorLength method - Validates if the vector is of the specified length
     * @param vector
     * @param name
     * @param length
     * Throws an exception if the vector is not of the specified length
     */
    private void validateVectorLength(double[] vector, String name, int length){
        if (vector.length != length){
            throw new IllegalArgumentException(name + " must have " + length + " dimensions");
        }
    }

    /**
     * validateVectorValues method - Validates if the vector contains NaN or Infinity values
     * @param vector
     * @param name
     * Throws an exception if the vector contains NaN or Infinity values
     */
    private void validateVectorValues(double[] vector, String name){
        for (double value : vector) {    
            if (Double.isNaN(value) || Double.isInfinite(value)) {
                throw new IllegalArgumentException(name + " must not contain NaN or Infinity values");
            }
        }
    }

    /**
     * calculateArea method - Calculates the area of the projectile
     * @return area of the projectile
     */
    private double calculateArea(){
        return Math.PI * Math.pow(radius, 2);           //Assuming the projectile is a sphere
    }


    /**
     * calculateDragForce method - Calculates the drag force acting on the projectile
     * @return drag force vector
     */
    private double[] calculateDragForce(){
        double flowSpeed = Math.sqrt(Math.pow(flowVelocity[0], 2) + Math.pow(flowVelocity[1], 2) + Math.pow(flowVelocity[2], 2));

        double dragMagnitude = -0.5 * fluidDensity * Math.pow(flowSpeed, 2) * dragCoefficient * calculateArea();

        return VectorProperties.Scale(flowVelocity, dragMagnitude);
    }

    /**
     * calculateTotalForce method - Calculates the total force acting on the projectile
     * @return total force vector
     */
    public double[] calculateTotalForce(){
        double[] gravityForce = VectorProperties.Scale(gravity, mass);
        double[] dragForce = calculateDragForce();

        double[] totalForce=  VectorProperties.Add(gravityForce, dragForce);
        return VectorProperties.Add(totalForce, force);
    }

    /**
     * updateValues method - Updates the velocity and position of the projectile
     * @param timestemp   - time interval
     * 
     * Prints the updated velocity and position
     */
    public void updateValues(double timestemp)
    {
        double[] totalForce= calculateTotalForce();
        for (int i=0;i<3;i++){
           
            velocity[i] += totalForce[i] / mass * timestemp;     // update velocity
            position[i] += velocity[i] * timestemp;             // update position
            printValues();
        }
    }

    /**
     * printValues method - Prints the velocity and position of the projectile
     */
    public void printValues(){
        System.out.println("Velocity: (" + velocity[0] + "," + velocity[1] + "," + velocity[2] + ")");
        System.out.println("Position: (" + position[0] + "," + position[1] + "," + position[2]+ ")");
    }

    /**
     * get methods
     */
    public double[] getVelocity() {
        return velocity;
    }

    public double[] getPosition() {
        return position;
    }

    public double[] getGravity() {
        return gravity;
    }

    public double[] getForce() {
        return force;
    }

    public double[] getFlowVelocity() {
        return flowVelocity;
    }

    public double getMass() {
        return mass;
    }

    public double getRadius() {
        return radius;
    }

    public double getDragCoefficient() {
        return dragCoefficient;
    }

    public double getFluidDensity() {
        return fluidDensity;
    }

}
