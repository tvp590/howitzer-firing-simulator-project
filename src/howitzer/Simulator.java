package howitzer;

/**
 * Simulator class - Simulates a projectile in different fluids
 * 
 *
 */

public class Simulator {
    public static void main(String[] args) {
        // Create the simulators for each fluid
        WaterSimulator waterSimulator = new WaterSimulator();
        AirSimulator airSimulator = new AirSimulator();
        ThickLiquidSimulator thickLiquidSimulator = new ThickLiquidSimulator();
        
         
        /**
         * Create a Projectile with valid input values in air
         */
        double[] airInitialPosition = {0.0, 0.0, 0.0};
        double[] airInitialVelocity = {50.0, 50.0, 40.0};
        double mass = 10.0;
        double radius = 0.1;   //radius = 0.1m
        double[] gravity = {0.0, 0.0, -9.81};
        double dragCoefficient = 0.47;
        double airfluidDensity = 1.225;
        double[] airFlowVelocity = {0.0, 0.0, 0.0};
        double[] airForce = {0.0, 0.0, 0.0};
        airSimulator.runSimulator(airInitialPosition,airInitialVelocity,mass,airFlowVelocity,airForce,radius,dragCoefficient,airfluidDensity,gravity);

        /**
         * Create a Projectile with valid input values in water
         */
        double[] waterInitialPosition = {0.0, 0.0, 0.0};
        double[] waterInitialVelocity = {50.0, 50.0, 40.0};
        double[] waterFlowVelocity = {1.0, 0.0, 0.0};
        double[] waterForce = {1.0, 0.0, 0.0};
        double waterfluidDensity = 100.0;
        waterSimulator.runSimulator(waterInitialPosition,waterInitialVelocity,mass,waterFlowVelocity,waterForce,radius,dragCoefficient,waterfluidDensity,gravity);
        
        /**
         * Create a Projectile with valid input values in thick liquid
         */
        double[] thickLiquidInitialPosition = {0.0, 0.0, 0.0};
        double[] thickLiquidInitialVelocity = {50.0, 50.0, 40.0};
        double[] thickLiquidFlowVelocity = {0.0, 3.0, 0.0};
        double[] thickLiquidForce = {5.0, 0.0, 0.0};
        double thickLiquidfluidDensity = 400.0;
        thickLiquidSimulator.runSimulator(thickLiquidInitialPosition,thickLiquidInitialVelocity,mass,thickLiquidFlowVelocity,thickLiquidForce,radius,dragCoefficient,thickLiquidfluidDensity,gravity);
    }
    
}
