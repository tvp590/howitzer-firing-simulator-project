package howitzer;

/**
 * VectorProperties class - Contains methods to perform operations on vectors
 * 
 *
 */
public class VectorProperties {

    /**
     * Scale method - Scales a vector by a scalar
     * @param v1    - Vector to be scaled
     * @param scalar- Scalar to scale the vector
     * @return    - Returns the scaled vector
     */
    public static double[] Scale(double[] v1, double scalar){
        double[] result = new double[v1.length];
        for (int i = 0; i < v1.length; i++) {
            result[i] = v1[i] * scalar;
        }
        return result;
    }

    /**
     * Add method - Adds two vectors
     * @param v1  - Vector 1
     * @param v2  - Vector 2
     * @return  - Returns the sum of the two vectors
     */
    public static double[] Add(double[] v1, double[] v2){
        double[] result = new double[v1.length];
        for (int i = 0; i < v1.length; i++) {
            result[i] = v1[i] + v2[i];
        }
        return result;
    }

    /**
     * isZeroVector method - Checks if the vector is a zero vector
     * @param v1    - Vector to be checked
     * @return      - Returns true if the vector is a zero vector
     */
    public static boolean isZeroVector(double[] v1){
        for (int i = 0; i < v1.length; i++) {
            if (v1[i] != 0.0){
                return false;
            }
        }
        return true;
    }
}
