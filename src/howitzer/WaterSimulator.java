package howitzer;

/**
 * WaterSimulator class - Contains methods to simulate a projectile in water
 */
public class WaterSimulator {

    /**
     * runSimulator method - Runs the simulation of a projectile in water
     * @param initialPosition  - Initial position of the projectile
     * @param initialVelocity  - Initial velocity of the projectile
     * @param mass             - Mass of the projectile 
     * @param flowVelocity     - Velocity of the fluid
     * @param force            - Force acting on the projectile
     * @param radius           - Radius of the projectile
     * @param dragCoefficient  - Drag coefficient
     * @param fluidDensity     - Density of the fluid
     * @param gravity          - Gravity acting on the projectile
     * @return                 - Returns the projectile object after the simulation
     */
    public Projectile runSimulator(double[] initialPosition, double[] initialVelocity, double mass, double[] flowVelocity, double[] force, double radius, double dragCoefficient, double fluidDensity, double[] gravity){
        Projectile projectile = new Projectile(mass, initialVelocity, initialPosition, gravity, force, dragCoefficient, fluidDensity, flowVelocity, radius);

        SimulateProjectile(projectile,"Water");
        return projectile;
    }

    /**
     * SimulateProjectile method - Simulates the projectile in the fluid
     * @param projectile    - Projectile object
     * @param fluidType     - Type of fluid
     * 
     * This method simulates the projectile in the fluid and prints the initial and final values of the projectile
     */
    private void SimulateProjectile(Projectile projectile, String fluidType){
        double timestemp = 0.1;
        int steps = 20;

        System.out.println("Projectile in " + fluidType + " simulation");
        System.out.println("Initial values:");
        projectile.printValues();

        for (int i = 0; i < steps; i++) {
            projectile.updateValues(timestemp);
        }

        System.out.println("Final values:");
        projectile.printValues();
        System.out.println("Simulation in " + fluidType + " completed\n");
    }
}
