package test;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import howitzer.Projectile;

/**
 * The class <code>BoundaryTests</code> contains tests for the class <code>{@link Projectile}</code>.
 * 
 *This class tests the boundary values of the input parameters of the constructor of the Projectile class
 *It mainly tests Mass, Radius, Drag Coefficient and Fluid Density.
 */
public class BoundaryTests {
    Projectile projectile;
      double mass;
        double[] initialPosition;
        double[] initialVelocity;
        double[] gravity;
        double[] force;
        double dragCoefficient;
        double fluidDensity;
        double[] flowVelocity;
        double radius;

    /**
     * Sets up the test fixture.
     * @throws Exception
     */
    @Before
    public void setup()throws Exception{
        mass = 1;
        initialPosition = new double[] {0.0, 0.0, 0.0};
        initialVelocity = new double[] {50.0, 30.0, 40.0};
        gravity = new double[] {0.0, 0.0, -9.81};
        force = new double[] {0.0, 0.0, 0.0};
        dragCoefficient = 0.47;
        fluidDensity = 1000.0;
        flowVelocity = new double[] {0.0, 0.0, 0.0};
        radius = 1;
    }

	@After
	public void tearDown() throws Exception {
	}

    /**
     * Test the invalid values of mass
     */
    @Test
    public void testInvalidValuesOfMass(){
        //mass is below the minimum limit
        try{
            projectile = new Projectile(-1, initialVelocity, initialPosition, gravity, force, dragCoefficient, fluidDensity, flowVelocity, radius);
            fail("Expected an IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), "Mass must be between " + Projectile.MIN_MASS + " and " + Projectile.MAX_MASS);
        }
        
        //mass =0
        try{
            projectile = new Projectile(0, initialVelocity, initialPosition, gravity, force, dragCoefficient, fluidDensity, flowVelocity, radius);
            fail("Expected an IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), "Mass must be between " + Projectile.MIN_MASS + " and " + Projectile.MAX_MASS);
        }

        // mass is above the maximum limit 
        try{
            projectile = new Projectile(100.50, initialVelocity, initialPosition, gravity, force, dragCoefficient, fluidDensity, flowVelocity, radius);
            fail("Expected an IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), "Mass must be between " + Projectile.MIN_MASS + " and " + Projectile.MAX_MASS);
        }

    }

    /**
     * Test the invalid values of Radius
     */
    @Test
    public void testInvalidValuesOfRadius(){
        //radius is below the minimum limit
        try{
            projectile = new Projectile(mass, initialVelocity, initialPosition, gravity, force, dragCoefficient, fluidDensity, flowVelocity, -1);
            fail("Expected an IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), "Radius must be between " + Projectile.MIN_RADIUS + " and " + Projectile.MAX_RADIUS);
        }

        //radius =0
        try{
            projectile = new Projectile(mass, initialVelocity, initialPosition, gravity, force, dragCoefficient, fluidDensity, flowVelocity, 0);
            fail("Expected an IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), "Radius must be between " + Projectile.MIN_RADIUS + " and " + Projectile.MAX_RADIUS);
        }

        // radius is above the maximum limit
        try{
            projectile = new Projectile(mass, initialVelocity, initialPosition, gravity, force, dragCoefficient, fluidDensity, flowVelocity, 100.50);
            fail("Expected an IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), "Radius must be between " + Projectile.MIN_RADIUS + " and " + Projectile.MAX_RADIUS);
        }

    }

    /**
     * Test the invalid values of DragCoefficient
     */
    @Test
    public void testInvalidValuesOfDragCoefficient(){
        //dragCoefficient is below the minimum limit
        try{
            projectile = new Projectile(mass, initialVelocity, initialPosition, gravity, force, -1, fluidDensity, flowVelocity, radius);
            fail("Expected an IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), "Drag coefficient must be between " + Projectile.MIN_DRAG_COEFFICIENT + " and " + Projectile.MAX_DRAG_COEFFICIENT);
        }

        //dragCoefficient =0
        try{
            projectile = new Projectile(mass, initialVelocity, initialPosition, gravity, force, 0, fluidDensity, flowVelocity, radius);
            fail("Expected an IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), "Drag coefficient must be between " + Projectile.MIN_DRAG_COEFFICIENT + " and " + Projectile.MAX_DRAG_COEFFICIENT);
        }

        // dragCoefficient is above the maximum limit
        try{
            projectile = new Projectile(mass, initialVelocity, initialPosition, gravity, force, 3.50, fluidDensity, flowVelocity, radius);
            fail("Expected an IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), "Drag coefficient must be between " + Projectile.MIN_DRAG_COEFFICIENT + " and " + Projectile.MAX_DRAG_COEFFICIENT);
        }

    }

    /**
     * Test the invalid values of FluidDensity
     */
    @Test
    public void testInvalidValuesOfFluidDensity(){
        //fluidDensity is below the minimum limit
        try{
            projectile = new Projectile(mass, initialVelocity, initialPosition, gravity, force, dragCoefficient, -1, flowVelocity, radius);
            fail("Expected an IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), "Fluid density must be between " + Projectile.MIN_FLUID_DENSITY + " and " + Projectile.MAX_FLUID_DENSITY);
        }

        //fluidDensity =0
        try{
            projectile = new Projectile(mass, initialVelocity, initialPosition, gravity, force, dragCoefficient, 0, flowVelocity, radius);
            fail("Expected an IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), "Fluid density must be between " + Projectile.MIN_FLUID_DENSITY + " and " + Projectile.MAX_FLUID_DENSITY);
        }
        
        // fluidDensity is above the maximum limit
        try{
            projectile = new Projectile(mass, initialVelocity, initialPosition, gravity, force, dragCoefficient, 5000.50, flowVelocity, radius);
            fail("Expected an IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), "Fluid density must be between " + Projectile.MIN_FLUID_DENSITY + " and " + Projectile.MAX_FLUID_DENSITY);
        }

    }

    

}
