package test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import howitzer.AirSimulator;
import howitzer.Projectile;
import howitzer.WaterSimulator;
import howitzer.ThickLiquidSimulator;

/**
 * Integration testing for the howitzer package
 * 
 * This class tests the interaction between the Projectile class and the
 * different simulator classes.
 * 
 * 
 */
public class IntegrationTesting {
    double mass;
        double[] initialPosition;
        double[] initialVelocity;
        double[] gravity;
        double[] force;
        double dragCoefficient;
        double fluidDensity;
        double[] flowVelocity;
        double radius;

    @Before
    public void setup()throws Exception{
        mass = 1.0;
        initialVelocity = new double[]{ 10.0, 0.0, 0.0 };
        initialPosition = new double[]{ 0.0, 0.0, 0.0 };
        gravity = new double[]{ 0.0, 0.0, -9.81 };
        force = new double[]{ 0.0, 0.0, 0.0 };
        dragCoefficient = 0.47;
        flowVelocity = new double[]{ 1.0, 0.0, 0.0 };
        radius = 1.0;
    }

	@After
	public void tearDown() throws Exception {
	}
    
    /**
     * Test the interaction between the AirSimulator and the Projectile class
     * use ArrayEquals to compare Projectile's velocity and position with the Result's velocity and position of the AirSimulator
     */
    @Test
    public void testAirSimulatorAndProjectileInteraction() {
        // Create a Projectile with valid input values
        double fluidDensity = 1.225;

        Projectile projectile = new Projectile(mass, initialVelocity, initialPosition, gravity, force,
                dragCoefficient, fluidDensity, flowVelocity, radius);

        // Create an AirSimulator and run the simulation
        AirSimulator airSimulator = new AirSimulator();
        Projectile result = airSimulator.runSimulator(initialPosition, initialVelocity, mass, flowVelocity, force, radius, dragCoefficient, fluidDensity, gravity);

        // Verify that the simulation updates the Projectile correctly
        assertArrayEquals(projectile.getVelocity(), result.getVelocity(), 0.0001);
        assertArrayEquals(projectile.getPosition(), result.getPosition(), 0.0001);
    }

    /**
     * Test the interaction between the WaterSimulator and the Projectile class
     * use ArrayEquals to compare Projectile's velocity and position with the Result's velocity and position of the WaterSimulator
     */
    @Test
    public void testWaterSimulatorAndProjectileInteraction() {
        // Create a Projectile with valid input values
        double fluidDensity = 10.0;

        Projectile projectile = new Projectile(mass, initialVelocity, initialPosition, gravity, force,
                dragCoefficient, fluidDensity, flowVelocity, radius);

        // Create a WaterSimulator and run the simulation
        WaterSimulator waterSimulator = new WaterSimulator();
        Projectile result = waterSimulator.runSimulator(initialPosition, initialVelocity, mass, flowVelocity, force, radius, dragCoefficient, fluidDensity, gravity);

        // Verify that the simulation updates the Projectile correctly
        assertArrayEquals(projectile.getVelocity(), result.getVelocity(), 0.0001);
        assertArrayEquals(projectile.getPosition(), result.getPosition(), 0.0001);
    }

    /**
     * Test the interaction between the ThickLiquidSimulator and the Projectile class
     * use ArrayEquals to compare Projectile's velocity and position with the Result's velocity and position of the ThickLiquidSimulator
     */
    @Test
    public void testthickLiquidSimulatorAndProjectileIneraction(){
        // Create a Projectile with valid input values
        double fluidDensity = 40.0;
        Projectile projectile = new Projectile(mass, initialVelocity, initialPosition, gravity, force, dragCoefficient, fluidDensity, flowVelocity, radius);

        double timestemp = 0.1;
        int steps = 10;
        for (int i = 0; i < steps; i++) {
            projectile.updateValues(timestemp);
        }

        // Create a WaterSimulator and run the simulation
        ThickLiquidSimulator thickLiquidSimulator = new ThickLiquidSimulator();
        Projectile result = thickLiquidSimulator.runSimulator(initialPosition, initialVelocity, mass, flowVelocity, force, radius,dragCoefficient, fluidDensity, gravity);

        // Verify that the simulation updates the Projectile correctly
        assertArrayEquals(projectile.getVelocity(), result.getVelocity(), 0.0001);
        assertArrayEquals(projectile.getPosition(), result.getPosition(), 0.0001);
    }

}

