package test;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import howitzer.Projectile;
import howitzer.VectorProperties;

/**
 * PathTestings - Test the path of the projectile
 *this class tests the path of the projectile with valid and invalid input values
 *
 */
public class PathTestings {
    Projectile projectile;
    private double[] validInitialVelocity;
    private double[] validGravity;
    private double[] validForce;
    private double[] validFlowVelocity;

    private double[] invalidInitialVelocity;
    private double[] invalidGravity;
    private double[] invalidForce;
    private double[] invalidFlowVelocity;
    private double mass;
    private double radius;
    private double dragCoefficient;
    private double fluidDensity;
    private double[] initialPosition;

    /**
     * setup - setup the initial values for the projectile
     * @throws Exception
     */
    @Before
    public void setup() throws Exception {
        validInitialVelocity = new double[] { 10.0, 30.0, 40.0 };
        validGravity = new double[] { 0.0, 0.0, -9.81 };
        validForce = new double[] { 5.0, 2.0, 0.0 };
        validFlowVelocity = new double[] { 1.0, 1.0, 0.0 };

        invalidInitialVelocity = new double[] { 0.0, 0.0, 0.0 }; // initial velocity should not be zero
        invalidGravity = new double[] { 0.0, 0.0, 0.0 }; // gravity should not be zero
        invalidForce = new double[] { 10.0, 2.0 }; // invalid dimensions
        invalidFlowVelocity = new double[] { -1.0, 0.0 }; // invalid dimensions

        mass = 1;
        radius = 1;
        dragCoefficient = 0.47;
        fluidDensity = 1000.0;
        initialPosition = new double[] { 1.0, 2.0, 3.0 };
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * testvalidValuePath - test the path of the projectile with valid input values
     * Test Path followes : 
     * 
     *   1. Valid Initialization of Projectile variables
     *   2. Valid Update call
     *   3. this requirs valid Total Force Calculation
     *   4. requires valid Drag Force Calculation
     *   5. requires valid Area Calculation
     *   6. print updated position and velocity
     * 
     * It uses AssertArrayEquals to check the values of the arrays valocity and position with the values of updated velocity and position of the projectile
     */
    @Test
    public void testValidValuePath() {
        try {
            projectile = new Projectile(mass, validInitialVelocity, initialPosition, validGravity, validForce,
                    dragCoefficient, fluidDensity, validFlowVelocity, radius);
            assertNotNull(projectile);

            assertArrayEquals(projectile.getVelocity(), validInitialVelocity, 0.0001); // Allow tolerance of 0.0001
            assertArrayEquals(projectile.getGravity(), validGravity, 0.0001);
            assertArrayEquals(projectile.getForce(), validForce, 0.0001);
            assertArrayEquals(projectile.getFlowVelocity(), validFlowVelocity, 0.0001);

            // Test updateValues method
            double timestep = 0.1;
            projectile.updateValues(timestep);

            // Since no force is applied, the velocity should remain unchanged
            assertArrayEquals(projectile.getVelocity(), validInitialVelocity, 0.0001);
           
            // double[] totalForce = projectile.calculateTotalForce();
            double flowSpeed = Math.sqrt(Math.pow(validFlowVelocity[0], 2) + Math.pow(validFlowVelocity[1], 2) + Math.pow(validFlowVelocity[2], 2));
            double dragMagnitude = -0.5 * fluidDensity * Math.pow(flowSpeed, 2) * dragCoefficient * Math.PI * Math.pow(radius, 2);
            double[] dragForce = VectorProperties.Scale(validFlowVelocity, dragMagnitude);
            double[] gravityForce = VectorProperties.Scale(validGravity, mass);

            double[] totalForce=  VectorProperties.Add(gravityForce, dragForce);
            totalForce =  VectorProperties.Add(totalForce, validForce);
            
            for (int i = 0; i < 3; i++) {
                validInitialVelocity[i] += (totalForce[i] / mass) * timestep;
                initialPosition[i]+= (validInitialVelocity[i] * timestep);
            }

            assertArrayEquals(projectile.getPosition(), initialPosition, 0.0001);
            assertArrayEquals(projectile.getVelocity(), validInitialVelocity, 0.0001);
        } catch (IllegalArgumentException e) {
            fail("Expected no IllegalArgumentException to be thrown");
        }
    }

    /**
     * testInvalidValuePath - test the path of the projectile with invalid input values
     * Test Path followes :
     *  1. Any or all invalid Initialization of Projectile variables
     *  2. validateValues() method call
     *  3. This validateValue() method found any or all invalid values then it throws IllegalArgumentException 
     *  4. Code catch the exception and close the program
     */
    @Test
    public void testInvalidValuePath() {
        // Invalid initial velocity
        try {
            projectile = new Projectile(mass, invalidInitialVelocity, initialPosition, validGravity, validForce,
                    dragCoefficient, fluidDensity, validFlowVelocity, radius);
            fail("Expected an IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), "Velocity must not be a zero vector");
        }

        // Invalid gravity
        try {
            projectile = new Projectile(mass, validInitialVelocity, initialPosition, invalidGravity, validForce,
                    dragCoefficient, fluidDensity, validFlowVelocity, radius);
            fail("Expected an IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), "Gravity must not be a zero vector");
        }

        // Invalid force
        try {
            projectile = new Projectile(mass, validInitialVelocity, initialPosition, validGravity, invalidForce,
                    dragCoefficient, fluidDensity, validFlowVelocity, radius);
            fail("Expected an IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), "Force must have 3 dimensions");
        }

        // Invalid flow velocity
        try {
            projectile = new Projectile(mass, validInitialVelocity, initialPosition, validGravity, validForce,
                    dragCoefficient, fluidDensity, invalidFlowVelocity, radius);
            fail("Expected an IllegalArgumentException to be thrown");
        } catch (IllegalArgumentException e) {
            assertEquals(e.getMessage(), "Flow velocity must have 3 dimensions");
        }
    }
}
